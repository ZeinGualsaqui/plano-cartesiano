import pygame

celeste = (183,239, 205)
rosa=(255, 132, 124)
casiblanco=(238, 236, 218)
negro=(0,0,0)

pygame.init()
Dimensiones = (600, 600)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Tarea 4")

#CREO EL TEXTO DEL PLANO CARTESIANO
Fuente = pygame.font.Font(None, 20)
Texto = Fuente.render("Plano Cartesiano", True, negro)
X= Fuente.render("X", True, rosa)
Y = Fuente.render("Y", True, rosa)
Cuadrado =Fuente.render("a=6", True, rosa)
Uno = Fuente.render("1", True, rosa)
Dos = Fuente.render("2", True, rosa)
Uno1 = Fuente.render("1", True, rosa)
Dos2 = Fuente.render("2", True, rosa)
Var = Fuente.render("x", True, rosa)
Var2 = Fuente.render("y", True, rosa)


Terminar = False

while not Terminar:
     for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True

     Pantalla.fill(casiblanco)
     pygame.draw.line(Pantalla, rosa, [595,300],[5,300],3)
     pygame.draw.line(Pantalla, rosa, [300, 5], [300, 595], 3)
     pygame.draw.rect(Pantalla, celeste, (300, 300, 250, 120), 0)
     Pantalla.blit(Texto, [10, 10])
     Pantalla.blit(X, [585, 280])
     Pantalla.blit(Y, [310, 585])
     Pantalla.blit(Uno, [290, 310])
     Pantalla.blit(Dos, [290, 340])
     Pantalla.blit(Uno1, [310, 280])
     Pantalla.blit(Dos2, [330, 280])
     Pantalla.blit(Var, [540, 280])
     Pantalla.blit(Var2, [275, 410])
     Pantalla.blit(Cuadrado, [400, 350])

     pygame.display.flip()

pygame.quit()
